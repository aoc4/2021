from day7 import day

DATA = [int(item) for item in "16,1,2,0,4,2,7,1,2,14".split(",")]


def test_example_solution1():
    expected = 37
    assert expected == day.solution1(DATA)


def test_example_solution2():
    expected = 168
    assert expected == day.solution2(DATA)
