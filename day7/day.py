from typing import Callable, List


def get_dist_sum(data: List[int], point: int) -> int:
    return sum(abs(point - i) for i in data)


def get_direction(data: List[int], avg: int, sum_func: Callable[[List[int], int], int]) -> int:
    step = avg - 1
    avg_sum = sum_func(data, avg)
    step_sum = sum_func(data, step)
    return -1 if avg_sum > step_sum else 1


def solution1(raw_data: List[int], sum_func: Callable[[List[int], int], int]=get_dist_sum) -> int:
    data = sorted(raw_data)
    avg_point = sum(data) // len(data)
    direction = get_direction(data, avg_point, sum_func)
    minimum = sum_func(data, avg_point)
    next_step = avg_point + direction
    while True:
        next_step_val = sum_func(data, next_step)
        if next_step_val < minimum:
            minimum = next_step_val
            next_step += direction
        else:
            break
    return minimum

def get_dist_cumsum(data: List[int], point: int) -> int:
    return sum(sum(range(abs(point - i) + 1)) for i in data)


def solution2(raw_data: List[int]) -> int:
    return solution1(raw_data, get_dist_cumsum)
