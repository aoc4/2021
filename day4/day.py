from typing import List, Tuple


def parse_matrix(data: str) -> List[List[int]]:
    return [
        [int(item) for item in line.split()]
        for line in data.splitlines()
    ]


def parse_data(data: str) -> Tuple[List[int], List[List[int]]]:
    parts = data.split("\n\n")
    draws = [int(part) for part in parts[0].split(",")]
    boards = [parse_matrix(part) for part in parts[1:]]
    return draws, boards


def is_sublist(candidate: List[int], main: List[int]) -> bool:
    matches = [item for item in candidate if item in main]
    return len(matches) == len(candidate)


def board_wins(board: List[List[int]], draws: List[int]) -> bool:
    # horizontal
    for row in board:
        if is_sublist(row, draws):
            return True

    # vertical
    for col_idx in range(len(board[0])):
        col_values = [row[col_idx] for row in board]
        if is_sublist(col_values, draws):
            return True

    return False


def get_unmarked_sum(board: List[List[int]], draws: List[int]):
    return sum(sum(item for item in line if item not in draws) for line in board)


def solution1(data: str) -> int:
    draws, boards = parse_data(data)
    cummulative_draws = []
    for draw in draws:
        cummulative_draws.append(draw)
        for board in boards:
            if board_wins(board, cummulative_draws):
                unmarked_sum = get_unmarked_sum(board, cummulative_draws)
                return draw * unmarked_sum

    return -1


def solution2(data: str) -> int:
    draws, boards = parse_data(data)
    cummulative_draws = []
    for draw in draws:
        cummulative_draws.append(draw)
        for board in boards:
            if board_wins(board, cummulative_draws):
                if len(boards) == 1:
                    unmarked_sum = get_unmarked_sum(boards[0], cummulative_draws)
                    return draw * unmarked_sum
                else:
                    boards.remove(board)

    return -1
