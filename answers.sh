#!/bin/bash

for d in *; do
    if [ -d "$d" ]; then
        echo "Running $d";
        python -m $d.__init__
    fi
done
