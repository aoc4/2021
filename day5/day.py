from dataclasses import dataclass
from typing import List, Tuple


@dataclass
class Point:
    x: int
    y: int


def add_vent(start: Point, end: Point, ocean_floor: List[List[int]]) -> List[List[int]]:
    updated_floor = ocean_floor.copy()
    start_x, end_x = (start.x, end.x) if start.x < end.x else (end.x, start.x)
    start_y, end_y = (start.y, end.y) if start.y < end.y else (end.y, start.y)
    for x in range(start_x, end_x + 1):
        for y in range(start_y, end_y + 1):
            updated_floor[y][x] += 1
    return updated_floor


def add_diagonal_vent(start: Point, end: Point, ocean_floor: List[List[int]]) -> List[List[int]]:
    updated_floor = ocean_floor.copy()
    x_direction = 1 if start.x < end.x else -1
    y_direction = 1 if start.y < end.y else -1
    for x, y in zip(range(start.x, end.x + x_direction, x_direction), range(start.y, end.y + y_direction, y_direction)):
        updated_floor[y][x] += 1
    return updated_floor


def parse_and_build_start_end(data: str) -> Tuple[Point, Point]:
    start, end = data.split("->")
    start_x, start_y = [int(item) for item in start.split(",")]
    end_x, end_y = [int(item) for item in end.split(",")]
    return Point(start_x, start_y), Point(end_x, end_y)


def solution1(data: List[str]) -> int:
    ocean_floor = [[0 for _ in range(1000)] for __ in range(1000)]  # too lazy to make dynamic expanding solution

    for datapoint in data:
        start, end = parse_and_build_start_end(datapoint)
        if start.x == end.x or start.y == end.y:  # task is to only consider lines
            ocean_floor = add_vent(start, end, ocean_floor)

    return sum(sum(1 for point in line if point >= 2) for line in ocean_floor)


def solution2(data: List[str]) -> int:
    ocean_floor = [[0 for _ in range(1000)] for __ in range(1000)]  # too lazy to make dynamic expanding solution

    for datapoint in data:
        start, end = parse_and_build_start_end(datapoint)
        if start.x == end.x or start.y == end.y:  # task is to only consider lines
            ocean_floor = add_vent(start, end, ocean_floor)
        elif (start.x == start.y and end.x == end.y) or (start.x == end.y and start.y == end.x) or (abs(end.x - start.x) == abs(end.y - start.y)):
            ocean_floor = add_diagonal_vent(start, end, ocean_floor)

    return sum(sum(1 for point in line if point >= 2) for line in ocean_floor)
