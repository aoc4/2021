from day5 import day

DATA = """0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
""".splitlines()


def test_example_solution1():
    expected = 5
    assert expected == day.solution1(DATA)


def test_example_solution2():
    expected = 12
    assert expected == day.solution2(DATA)
