from multiprocessing.sharedctypes import Value
from typing import List, Set, Tuple


def parse_paper(data: str) -> Set[Tuple[int, int]]:
    # all we need is a set of points
    points = [point.split(",") for point in data.splitlines()]
    return set((int(x), int(y)) for x, y in points)


def parse_instructions(guidance: str) -> List[Tuple[str, int]]:
    instructions = []
    for line in guidance.splitlines():
        dimension, value = line.replace("fold along ", "").split("=")
        instructions.append((dimension, int(value)))
    return instructions


def fold_paper(paper: Set[Tuple[int, int]], fold_dimension: str, fold_point: int) -> Set[Tuple[int, int]]:
    new_points = set()
    if fold_dimension == "x":
        for x, y in paper:
            if x > fold_point:
                new_points.add((2 * fold_point - x, y))
            else:
                new_points.add((x, y))
    elif fold_dimension == "y":
        for x, y in paper:
            if y > fold_point:
                new_points.add((x, 2 * fold_point - y))
            else:
                new_points.add((x, y))
    else:
        raise ValueError(f"Unknown dimension {fold_dimension}")

    return new_points


def format_paper(paper: Set[Tuple[int, int]]) -> List[List[str]]:
    size_x = max(x for x, _ in paper) + 1
    size_y = max(y for _, y in paper) + 1
    formatted = [["." for _ in range(size_x)] for _ in range(size_y)]
    for x, y in paper:
        formatted[y][x] = "#"
    return formatted


def solution1(data: str) -> int:
    coordinates, guidance = data.split("\n\n")
    paper = parse_paper(coordinates)
    instructions = parse_instructions(guidance)
    for dimension, point in instructions:
        paper = fold_paper(paper, dimension, point)
        break  # only one instruction
    return len(paper)


def solution2(data: str) -> str:
    # simple visual inspection
    coordinates, guidance = data.split("\n\n")
    paper = parse_paper(coordinates)
    instructions = parse_instructions(guidance)
    for dimension, point in instructions:
        paper = fold_paper(paper, dimension, point)
    return "\n" + "\n".join("".join(line) for line in format_paper(paper))
