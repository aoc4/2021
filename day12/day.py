from copy import deepcopy
from dataclasses import dataclass, field
from typing import Dict, List, Set

START = "start"
END = "end"


@dataclass
class Path:
    chain: List[str] = field(default_factory=list, compare=True)
    visited_short: Set[str] = field(default_factory=set, compare=False)


def parse_rules(data: List[str]) -> Dict[str, List[str]]:
    rules = {}
    for line in data:
        a, b = line.split("-")
        if a in rules:
            rules[a].append(b)
        else:
            rules[a] = [b]
        if b in rules:
            rules[b].append(a)
        else:
            rules[b] = [a]
    return rules


def fork_paths(path: Path, caves: List[str]) -> List[Path]:
    paths = []
    for cave in caves:
        if cave == START:
            continue
        new_path = deepcopy(path)
        new_path.chain.append(cave)
        if cave.islower():
            new_path.visited_short.add(cave)
        paths.append(new_path)
    return paths


def already_visited_short(path: Path) -> bool:
    return any(path.chain.count(short) > 1 for short in path.visited_short)


def build_all_paths(rules: Dict[str, List[str]], violation_func=already_visited_short) -> List[Path]:
    finished_paths = []
    new_paths = [Path(chain=[START])]

    while len(new_paths) != 0:
        forked_paths: List[Path] = []

        # add all new paths
        for path in new_paths:
            forked_paths.extend(fork_paths(path, rules[path.chain[-1]]))

        # remove short path violations
        without_violations = [path for path in forked_paths if not violation_func(path)]

        # remove finished and add to finished_paths
        new_paths.clear()
        for path in without_violations:
            if path.chain[-1] == END:
                finished_paths.append(path)
            else:
                new_paths.append(path)

    return finished_paths


def solution1(data: List[str]) -> int:
    rules = parse_rules(data)
    paths = build_all_paths(rules)
    return len(paths)


def multiple_visited_more_than_twice(path: Path) -> bool:
    # more than one short path
    visited_twice = False
    for short in path.visited_short:
        if path.chain.count(short) == 2:
            if visited_twice:
                return True
            visited_twice = True
        elif path.chain.count(short) > 2:
            return True
    return False


def solution2(data: List[str]) -> int:
    rules = parse_rules(data)
    paths = build_all_paths(rules, violation_func=multiple_visited_more_than_twice)
    return len(paths)
