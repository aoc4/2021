from day12 import day

DATA1 = """start-A
start-b
A-c
A-b
b-d
A-end
b-end""".splitlines()

DATA2 = """dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc""".splitlines()

DATA3 = """fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW""".splitlines()


def test_example1_solution1():
    expected = 10
    assert expected == day.solution1(DATA1)


def test_example2_solution1():
    expected = 19
    assert expected == day.solution1(DATA2)


def test_example3_solution1():
    expected = 226
    assert expected == day.solution1(DATA3)


def test_example1_solution2():
    expected = 36
    assert expected == day.solution2(DATA1)


def test_example2_solution2():
    expected = 103
    assert expected == day.solution2(DATA2)


def test_example3_solution2():
    expected = 3509
    assert expected == day.solution2(DATA3)
