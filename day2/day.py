from dataclasses import dataclass, field
from typing import Callable, Dict, List


@dataclass
class Position:
    horizontal: int = 0
    depth: int = 0
    aim: int = 0


MODIFIERS1: Dict[str, Callable[[Position, int], Position]] = {
    "forward": lambda p, x: Position(horizontal=p.horizontal + x, depth=p.depth),
    "down": lambda p, x: Position(horizontal=p.horizontal, depth=p.depth + x),
    "up": lambda p, x: Position(horizontal=p.horizontal, depth=p.depth - x)
}

MODIFIERS2: Dict[str, Callable[[Position, int], Position]] = {
    "forward": lambda p, x: Position(horizontal=p.horizontal + x, depth=p.depth + p.aim * x, aim=p.aim),
    "down": lambda p, x: Position(horizontal=p.horizontal, depth=p.depth, aim=p.aim + x),
    "up": lambda p, x: Position(horizontal=p.horizontal, depth=p.depth, aim=p.aim - x)
}


def solution1(instructions: List[List[str]]) -> int:
    """Read the instructions to tell final position's hash.

    Hash is depth * horizontal length multiplication

    Args
        instructions (List[List[str]]): list of instructions, of form [{instruction}, {change}]

    Returns
        int: depth * horizontal_change

    """
    position = Position()
    for instruction, change in instructions:
        position = MODIFIERS1[instruction](position, int(change))

    return position.horizontal * position.depth


def solution2(instructions: List[str]) -> int:
    """Read the instructions to tell final position's hash.

    Hash is depth * horizontal length multiplication

    Args
        instructions (List[List[str]]): list of instructions, of form [{instruction}, {change}]

    Returns
        int: depth * horizontal_change

    """
    position = Position()
    for instruction, change in instructions:
        position = MODIFIERS2[instruction](position, int(change))

    return position.horizontal * position.depth
