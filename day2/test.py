from day2 import day

EXAMPLE_DATA = [item.split() for item in """forward 5
down 5
forward 8
up 3
down 8
forward 2
""".splitlines()]


def test_example_solution1():
    expected = 150
    assert expected == day.solution1(EXAMPLE_DATA)


def test_example_solution2():
    expected = 900
    assert expected == day.solution2(EXAMPLE_DATA)
