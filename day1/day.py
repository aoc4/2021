from typing import List


def solution1(data: List[int]) -> int:
    """Calculate the amount of times a depth measurement increases.

    Args
        data (List[int]): list of measurements

    Returns
        int: amount of times depth measurement increases in comparison to last

    """
    # Look at pairs and check that second value is bigger
    return sum(a < b for a, b in zip(data, data[1:]))


def solution2(data: List[int]) -> int:
    """Calculate the amount of times a 3-measurement window sum increases.

    Args
        data (List[int]): list of measurement

    Returns
        int: amount of times a 3-measurement window sum increases in comparison to last

    """
    # simply a moving window of 3 measurements
    window = list(zip(data, data[1:], data[2:]))  # can't use subscription otherwise
    return sum(sum(a) < sum(b) for a, b in zip(window, window[1:]))
