from day1 import day

data = [int(item) for item in """199
200
208
210
200
207
240
269
260
263""".splitlines()]


def test_example_solution1():
    expected = 7
    assert expected == day.solution1(data)


def test_example_solution2():
    expected = 5
    assert expected == day.solution2(data)
