from typing import List, Tuple
from itertools import product


def is_low_point(x: int, y: int, data: List[List[int]]) -> bool:
    up = max(0, y - 1)
    down = min(len(data) - 1, y + 1)
    left = max(0, x - 1)
    right = min(len(data[0]) - 1, x + 1)
    datapoint = data[y][x]
    return ((x, y) == (x, up) or datapoint < data[up][x]) and\
        ((x, y) == (x, down) or datapoint < data[down][x]) and\
        ((x, y) == (left, y) or datapoint < data[y][left]) and\
        ((x, y) == (right, y) or datapoint < data[y][right])


def solution1(data: List[List[int]]) -> int:
    risk_level = 0
    for y in range(len(data)):
        for x in range(len(data[0])):
            if is_low_point(x, y, data):
                risk_level += data[y][x] + 1
    return risk_level


def get_basin_size(low_point: Tuple[int, int], data: List[List[int]]) -> int:
    points = set([low_point])
    buffer = set([low_point])
    while buffer != set():
        new_buffer = set()
        for x, y in buffer:
            up = max(0, y - 1)
            down = min(len(data) - 1, y + 1)
            left = max(0, x - 1)
            right = min(len(data[0]) - 1, x + 1)
            # no equivalence check as it gets cleaned up later
            for new_x, new_y in ((x, up), (x, down), (left, y), (right, y)):
                if data[new_y][new_x] != 9:
                    new_buffer.update({(new_x, new_y)})
        buffer = new_buffer - points
        points.update(new_buffer)

    return len(points)


def solution2(data: List[List[int]]) -> int:
    # find all low points
    low_points = [(x, y) for x, y in product(range(len(data[0])), range(len(data))) if is_low_point(x, y, data)]
    basins = sorted([get_basin_size(low_point, data) for low_point in low_points], reverse=True)
    return basins[0] * basins[1] * basins[2]
