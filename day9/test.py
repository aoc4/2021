from day9 import day

DATA = [[int(height) for height in line] for line in """2199943210
3987894921
9856789892
8767896789
9899965678
""".splitlines()]


def test_example_solution1():
    expected = 15
    assert expected == day.solution1(DATA)


def test_example_solution2():
    expected = 1134
    assert expected == day.solution2(DATA)
