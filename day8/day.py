from typing import Dict, List, Set, Tuple


def parse_input_line(line: str) -> Tuple[List[str], List[str]]:
    first_part, second_part = line.split("|")
    return first_part.split(), second_part.split()


def solution1(data: List[str]) -> int:
    count = 0
    for line in data:
        _, second_part = parse_input_line(line)
        count += sum(1 for item in second_part if len(item) in [2, 3, 4, 7])
    return count


def deduct_numbers(values: List[str]) -> Dict[Set[str], int]:
    one = frozenset(next(value for value in values if len(value) == 2))
    four = frozenset(next(value for value in values if len(value) == 4))
    seven = frozenset(next(value for value in values if len(value) == 3))
    eight = frozenset(next(value for value in values if len(value) == 7))
    six = frozenset(next(value for value in values if len(value) == 6 and not one.issubset(frozenset(value))))
    nine = frozenset(next(value for value in values if len(value) == 6 and four.issubset(frozenset(value))))
    zero = frozenset(next(value for value in values if len(value) == 6 and frozenset(value) not in [six, nine]))
    three = frozenset(next(value for value in values if len(value) == 5 and one.issubset(value)))
    five = frozenset(next(value for value in values if len(value) == 5 and frozenset(value).issubset(six)))
    two = frozenset(next(value for value in values if len(value) == 5 and frozenset(value) not in [five, three]))
    return {
        one: 1,
        two: 2,
        three: 3,
        four: 4,
        five: 5,
        six: 6,
        seven: 7,
        eight: 8,
        nine: 9,
        zero: 0
    }


def decypher_number(dictionary: Dict[str, int], cyphered: List[str]) -> int:
    number = 0
    for value in cyphered:
        number *= 10  # shift left
        number += dictionary[frozenset(value)]
    return number


def solution2(data: List[str]) -> int:
    count = 0
    for line in data:
        numbers, cyphered_numbers = parse_input_line(line)
        dictionary = deduct_numbers(numbers)
        count += decypher_number(dictionary, cyphered_numbers)
    return count
