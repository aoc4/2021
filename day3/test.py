from day3 import day

DATA = """00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010""".splitlines()


def test_example_solution1():
    expected = 198
    assert expected == day.solution1(DATA)


def test_example_solution2():
    expected = 230
    assert expected == day.solution2(DATA)
