from typing import List


def invert(bin_num: str) -> str:
    return "".join("1" if char == "0" else "0" for char in bin_num)


def solution1(data: List[str]) -> int:
    counts = {}
    for line in data:
        for idx, char in enumerate(line):
            counts[idx] = counts.setdefault(idx, 0) + int(char)

    data_size = len(data)
    most_common = "".join(str(round(count / data_size)) for count in counts.values())
    least_common = invert(most_common)

    return int(most_common, 2) * int(least_common, 2)


def get_nth_sum(data: List[str], n: int) -> str:
    return sum(int(line[n]) for line in data)


def get_nth_most_common(data: List[str], n: int) -> str:
    sum = get_nth_sum(data, n)
    if sum / len(data) == 0.5:
        return "1"
    return str(round(sum / len(data)))


def get_nth_least_common(data: List[str], n: int) -> str:
    sum = get_nth_sum(data, n)
    if sum / len(data) == 0.5:
        return "0"
    return "1" if str(round(sum / len(data))) == "0" else "0"


def solution2(data: List[str]) -> int:
    most_common_dataset = data.copy()
    least_common_dataset = data.copy()
    for idx in range(len(data)):
        nth_most_common = get_nth_most_common(data=most_common_dataset, n=idx)
        nth_least_common = get_nth_least_common(data=least_common_dataset, n=idx)
        # filter out data that has nth_most_common value at idx location
        if len(most_common_dataset) != 1:
            most_common_dataset = [item for item in most_common_dataset if item[idx] == nth_most_common]
        if len(least_common_dataset) != 1:
            least_common_dataset = [item for item in least_common_dataset if item[idx] == nth_least_common]
        if len(most_common_dataset) == 1 and len(least_common_dataset) == 1:
            break

    return int(least_common_dataset[0], 2) * int(most_common_dataset[0], 2)
