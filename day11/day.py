from copy import deepcopy
from typing import List, Set, Tuple

from itertools import product


def get_dimensions(board: List[List[int]]) -> Tuple[int, int]:
    return len(board[0]), len(board)


def adjacent_points(x: int, y: int, dim_x: int, dim_y: int) -> List[Tuple[int, int]]:
    """Retrieve points that will be affected by a flash."""
    bottom_y = min(dim_y - 1, y + 1)
    top_y = max(0, y - 1)
    left_x = max(0, x - 1)
    right_x = min(dim_x - 1, x + 1)
    top_left = (left_x, top_y)
    left = (left_x, y)
    bottom_left = (left_x, bottom_y)
    bottom = (x, bottom_y)
    bottom_right = (right_x, bottom_y)
    right = (right_x, y)
    top_right = (right_x, top_y)
    top = (x, top_y)
    return list(set([
        top_left,
        left,
        bottom_left,
        bottom,
        bottom_right,
        right,
        top_right,
        top
    ]))


def get_flashes(board: List[List[int]]) -> Set[Tuple[int, int]]:
    dim_x, dim_y = get_dimensions(board)
    return {(x, y) for x, y in product(range(dim_x), range(dim_y)) if board[y][x] > 9}


def get_updated_board(board: List[List[int]]) -> List[List[int]]:
    next_board = deepcopy(board)
    dim_x, dim_y = get_dimensions(board)
    for x, y in product(range(dim_x), range(dim_y)):
        next_board[y][x] += 1
    flashed_points = set()
    flashes = get_flashes(next_board)
    while not flashes.issubset(flashed_points):
        flashed_points.update(flashes)
        for x, y in flashes:
            adjacent = adjacent_points(x=x, y=y, dim_x=dim_x, dim_y=dim_y)
            for adj_x, adj_y in adjacent:
                next_board[adj_y][adj_x] += 1
        flashes = get_flashes(next_board) - flashed_points
    return next_board


def reset_flashed(board: List[List[int]]) -> List[List[int]]:
    dim_x, dim_y = get_dimensions(board)
    for x, y in product(range(dim_x), range(dim_y)):
        if board[y][x] > 9:
            board[y][x] = 0
    return board


def solution1(data: List[List[int]], steps: int) -> int:
    board = deepcopy(data)
    flashes = 0
    for _ in range(steps):
        new_board = get_updated_board(board)
        flashes += len(get_flashes(new_board))
        board = reset_flashed(new_board)
    return flashes


def solution2(data: List[List[int]]) -> int:
    board = deepcopy(data)
    dim_x, dim_y = get_dimensions(board)
    octopus_count = dim_x * dim_y
    steps = 0
    flashes = 0
    while flashes != octopus_count:
        new_board = get_updated_board(board)
        flashes = len(get_flashes(new_board))
        board = reset_flashed(new_board)
        steps += 1
    return steps
