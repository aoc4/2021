from day11 import day

DATA = [[int(item) for item in line] for line in """5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526
""".splitlines()]


def test_example_solution1():
    expected = 1656
    assert expected == day.solution1(DATA, steps=100)


def test_example_solution1_small():
    datapoint = [[int(item) for item in line] for line in"""11111
19991
19191
19991
11111""".splitlines()]
    expected = 9
    assert expected == day.solution1(datapoint, steps=1)


def test_example_solution1_step1():
    expected = 0
    assert expected == day.solution1(DATA, steps=1)


def test_example_solution1_step2():
    expected = 35
    assert expected == day.solution1(DATA, steps=2)


def test_example_solution1_step3():
    expected = 35 + 45
    assert expected == day.solution1(DATA, steps=3)

def test_example_solution1_step10():
    expected = 204
    assert expected == day.solution1(DATA, steps=10)

def test_example_solution2():
    expected = 195
    assert expected == day.solution2(DATA)
