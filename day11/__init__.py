from pathlib import Path

from day11 import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        data = [[int(item) for item in line] for line in f.read().splitlines()]

    print(f"solution1: {day.solution1(data, steps=100)}")
    print(f"solution2: {day.solution2(data)}")
