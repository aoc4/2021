#!/bin/bash

DAY=day$1
echo Creating ${DAY}

mkdir ${DAY}

cat << EOF > ${DAY}/__init__.py
from pathlib import Path

from ${DAY} import day

if __name__ == "__main__":
    input_path = Path(__file__).parent
    with open(input_path / "input.txt") as f:
        data = [item for item in f.read().splitlines()]

    print(f"solution1: {day.solution1(data)}")
    print(f"solution2: {day.solution2(data)}")
EOF

cat << EOF > ${DAY}/day.py
from typing import List


def solution1(data: List[str]) -> int:
    return -1


def solution2(data: List[str]) -> int:
    return -1
EOF

touch ${DAY}/input.txt

cat << EOF > ${DAY}/test.py
from ${DAY} import day

DATA = """
""".splitlines()


def test_example_solution1():
    expected = 123
    assert expected == day.solution1(DATA)


def test_example_solution2():
    expected = 456
    assert expected == day.solution2(DATA)
EOF