from typing import List, Tuple


def get_first_illegal_character(line: str) -> str:
    PAIRS = [
        ('(', ')'),
        ('[', ']'),
        ('{', '}'),
        ('<', '>')
    ]
    context_stack = []
    for char in line:
        for open_char, close_char in PAIRS:
            if char == open_char:
                context_stack.append((open_char, close_char))
                break
            elif char == close_char:
                if context_stack == []:
                    return char
                _, context_close = context_stack.pop()
                if context_close != char:
                    return char
                break
    return ''


def solution1(data: List[str]) -> int:
    POINTS = {
        '': 0,  # no errors
        ')': 3,
        ']': 57,
        '}': 1197,
        '>': 25137
    }

    return sum(POINTS[get_first_illegal_character(line)] for line in data)


def get_unfinished_context_stack(line: str) -> str:
    PAIRS = [
        ('(', ')'),
        ('[', ']'),
        ('{', '}'),
        ('<', '>')
    ]
    context_stack = []
    for char in line:
        for open_char, close_char in PAIRS:
            if char == open_char:
                context_stack.append((open_char, close_char))
                break
            elif char == close_char:
                if context_stack == []:
                    return []
                _, context_close = context_stack.pop()
                if context_close != char:
                    return []
                break
    return context_stack


def get_unclosed_chars(line: str) -> List[str]:
    context_stack = get_unfinished_context_stack(line)
    return list(reversed([close_char for _, close_char in context_stack]))


def get_score(chars: List[str]) -> int:
    POINTS = {
        ')': 1,
        ']': 2,
        '}': 3,
        '>': 4
    }
    score = 0
    for char in chars:
        score *= 5
        score += POINTS[char]
    return score


def solution2(data: List[str]) -> int:
    unclosed_chars = [get_unclosed_chars(line) for line in data]
    filtered_unclosed_char = [unclosed for unclosed in unclosed_chars if unclosed != []]
    scores = sorted([get_score(chars) for chars in filtered_unclosed_char])
    return scores[len(scores) // 2]
