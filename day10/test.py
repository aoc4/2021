from day10 import day

DATA = """[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
""".splitlines()


def test_example_solution1():
    expected = 26397
    assert expected == day.solution1(DATA)


def test_example_solution2():
    expected = 288957
    assert expected == day.solution2(DATA)

def test_example_solution2_1():
    datapoint = ["[({(<(())[]>[[{[]{<()<>>"]
    expected = 288957
    assert expected == day.solution2(datapoint)


def test_example_solution2_2():
    datapoint = ["[(()[<>])]({[<{<<[]>>("]
    expected = 5566
    assert expected == day.solution2(datapoint)


def test_example_solution2_3():
    datapoint = ["(((({<>}<{<{<>}{[]{[]{}"]
    expected = 1480781
    assert expected == day.solution2(datapoint)


def test_example_solution2_4():
    datapoint = ["{<[[]]>}<{[{[{[]{()[[[]"]
    expected = 995444
    assert expected == day.solution2(datapoint)


def test_example_solution2_5():
    datapoint = ["<{([{{}}[<[[[<>{}]]]>[]]"]
    expected = 294
    assert expected == day.solution2(datapoint)
