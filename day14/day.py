from collections import Counter, defaultdict
from itertools import pairwise
from typing import Dict


def parse_polymer(polymer_part: str) -> Dict[str, int]:
    """As we care about expansions, just parse this as all pair combinations."""
    polymer = defaultdict(int)
    for a, b in pairwise(polymer_part):
        pair = a + b
        polymer[pair] += 1
    return polymer


def parse_rules(rule_part: str) -> Dict[str, str]:
    rules = {}
    for line in rule_part.splitlines():
        left, right = line.split(" -> ")
        rules[left] = right
    return rules


def next_polymer(polymer: Dict[str, int], rules: Dict[str, str]) -> Dict[str, int]:
    """for every known pair we calculate mutation and pass the amount of times the mutation happens."""
    mutation = defaultdict(int)
    for pair, amount in polymer.items():
        a = pair[0] + rules[pair]
        b = rules[pair] + pair[1]
        mutation[a] += amount
        mutation[b] += amount
    return mutation


def get_elements_counts(polymer: Dict[str, int], initial_polymer: str) -> Dict[str, int]:
    elements = defaultdict(int)
    for pair, amount in polymer.items():
        elements[pair[0]] += amount
        elements[pair[1]] += amount

    # since it's pairs, everything but the edges are doubled
    elements[initial_polymer[0]] += 1
    elements[initial_polymer[-1]] += 1

    for char in elements:
        elements[char] = round(elements[char] / 2)
    return elements


def solution1(data: str, steps: int = 10) -> int:
    polymer_part, rules_part = data.split("\n\n")
    polymer = parse_polymer(polymer_part)
    rules = parse_rules(rules_part)
    for _ in range(steps):
        polymer = next_polymer(polymer, rules)

    most_common = Counter(get_elements_counts(polymer, polymer_part)).most_common()
    return most_common[0][1] - most_common[-1][1]


def solution2(data: str) -> int:
    return solution1(data, steps=40)
