from day14 import day

DATA = """NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C
"""


def test_example_solution1():
    expected = 1588
    assert expected == day.solution1(DATA)


def test_example_solution1_step1():
    expected = 1  # NCNBCHB
    assert expected == day.solution1(DATA, steps=1)


def test_example_solution1_step2():
    expected = 5  # NBCCNBBBCBHCB
    assert expected == day.solution1(DATA, steps=2)


def test_example_solution1_step3():
    expected = 7  # NBBBCNCCNBBNBNBBCHBHHBCHB
    assert expected == day.solution1(DATA, steps=3)


def test_example_solution1_step4():
    expected = 18  # NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB
    assert expected == day.solution1(DATA, steps=4)


def test_example_solution2():
    expected = 2188189693529
    assert expected == day.solution2(DATA)
