from typing import List


def solution1(data: List[int], days: int = 80) -> int:
    # approach: assign fishes to day groups
    fishes = [0 for _ in range(9)]
    for fish in data:
        fishes[fish] += 1

    for day in range(days):
        creators = fishes.pop(0)
        fishes.append(creators)
        fishes[6] += creators
    return sum(fishes)


def solution2(data: List[int]) -> int:
    return solution1(data, 256)
