from day6 import day

DATA = [int(item) for item in """3,4,3,1,2
""".split(",")]


def test_example_solution1():
    expected = 5934
    assert expected == day.solution1(DATA)


def test_example_solution2():
    expected = 26984457539
    assert expected == day.solution2(DATA)
